package com.example.newslisting.helper

import android.app.Application

class MoEngageApplication : Application() {


    init {
        moEngageApplication = this
    }

    companion object{
        var moEngageApplication: MoEngageApplication? = null
        fun getApplication(): MoEngageApplication? {
            return moEngageApplication
        }

    }


}