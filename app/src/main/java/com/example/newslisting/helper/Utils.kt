package com.example.newslisting.helper

import android.util.Log
import com.example.newslisting.model.Response
import com.google.gson.Gson
import java.io.InputStreamReader

class Utils {

    companion object {
        fun getNewsList(inputStreamReader: InputStreamReader?): Response? {
            return Gson().fromJson(inputStreamReader, Response::class.java)
        }

        fun convertToNewFormat(dateStr: String?): String? {
            try {
                return dateStr?.substringAfter("T")?.substringBefore("Z")
            } catch (e: Exception) {
                Log.d("Utils", "Error in displaying time")
            }
            return null
        }
    }
}