package com.example.newslisting.helper

import android.util.Log
import com.example.newslisting.model.Response
import java.io.InputStream
import java.io.InputStreamReader

class FetchNewsList {

    fun getNewsList(): Response? {
        val open: InputStream
        try {
            open = MoEngageApplication.getApplication()?.assets!!.open("news.json")
            return Utils.getNewsList(InputStreamReader(open))
        } catch (e: Exception) {
            Log.e("FetchNewsList", "Error loading json file"+e.message)
        }
        return null
    }
}