package com.example.newslisting.ui

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.newslisting.R
import com.example.newslisting.adapter.NewsListingAdapter
import com.example.newslisting.databinding.ActivityMainBinding
import com.example.newslisting.viewmodel.NewsListingViewModel
import com.google.firebase.messaging.FirebaseMessaging


class MainActivity : AppCompatActivity() {

    private lateinit var newsListAdapter : NewsListingAdapter

    private lateinit var binding: ActivityMainBinding

    private lateinit var newsListViewModel: NewsListingViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        newsListViewModel = ViewModelProvider(this).get(NewsListingViewModel::class.java)
        setContentView(binding.root)
        setToolBar()
        setAdapter()
        observeViewModel()
    }

    private fun setToolBar() {
        setSupportActionBar(binding.toolbar)
        binding.toolbar.showOverflowMenu()
        setThreeDotColor()
    }

    private fun observeViewModel() {
        newsListViewModel.fetchNewsList().observe(this, {
            newsListAdapter.setNewsList(it.articles)
        })
    }

    private fun setAdapter() {
        newsListAdapter = NewsListingAdapter()
        binding.rvNewsList.adapter = newsListAdapter
        binding.rvNewsList.layoutManager = LinearLayoutManager(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.overflow, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item_latest -> {
                if(!newsListViewModel.isLatestList()) {
                    setListType()
                }
                true
            }
            R.id.menu_item_reverse -> {
                if(newsListViewModel.isLatestList()){
                    setListType()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setListType() {
        newsListAdapter.reverseList()
        newsListViewModel.setListOrderType()
    }

    private fun setThreeDotColor() {
        var drawable = binding.toolbar.overflowIcon
        if (drawable != null) {
            drawable = DrawableCompat.wrap(drawable)
            DrawableCompat.setTint(drawable.mutate(), ContextCompat.getColor(this, R.color.white))
            binding.toolbar.overflowIcon = drawable
        }
    }
}