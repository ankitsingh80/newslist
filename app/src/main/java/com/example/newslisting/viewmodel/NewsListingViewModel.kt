package com.example.newslisting.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.newslisting.helper.FetchNewsList
import com.example.newslisting.model.Response
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.time.format.ResolverStyle


class NewsListingViewModel : ViewModel() {

    private val fetchNewsList = FetchNewsList()

    private val newsList = MutableLiveData<Response>()

    private var isLatestNews = true

     init {
         getNewsList()
     }

    private fun getNewsList() {
        viewModelScope.launch(Dispatchers.IO) {
            val data = fetchNewsList.getNewsList()
            newsList.postValue(data!!)
            isLatestNews = true
        }
    }

    fun isLatestList(): Boolean {
        return isLatestNews
    }

    fun fetchNewsList(): LiveData<Response> {
        return newsList
    }

    //toggling list when user sort the list by latest and oldest
    fun setListOrderType(): Boolean {
        isLatestNews = !isLatestNews
        return isLatestNews
    }
}