package com.example.newslisting.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.newslisting.R
import com.example.newslisting.helper.Utils
import com.example.newslisting.model.ArticlesItem


class NewsListingAdapter : RecyclerView.Adapter<NewsListingAdapter.NewsListViewHolder>() {

    private var articlesList = arrayListOf<ArticlesItem?>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.news_list, parent, false)
        return NewsListViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsListViewHolder, position: Int) {
        holder.bindData(articlesList[position])
    }

    override fun getItemCount() = articlesList.size

    inner class NewsListViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tvHeadline: TextView = itemView.findViewById(R.id.tvNewsheadLine)
        private var tvPublishTime: TextView = itemView.findViewById(R.id.tvPublishTime)
        private var tvDescription: TextView = itemView.findViewById(R.id.tvDescription)
        private var tvAuthor: TextView = itemView.findViewById(R.id.tvAuthor)
        private var ivNewsImage: ImageView = itemView.findViewById(R.id.ivNewsImage)

        fun bindData(item: ArticlesItem?) {
            tvHeadline.text = item?.title ?: ""
            tvPublishTime.text = Utils.convertToNewFormat(item?.publishedAt)?:""
            tvDescription.text = item?.description ?: ""
            tvAuthor.text = (item?.author + ", " + item?.source?.name)
            setNewsImage(item)
        }

        private fun setNewsImage(item: ArticlesItem?) {
            var requestOptions = RequestOptions()
            requestOptions = requestOptions.transform(CenterCrop(), RoundedCorners(16))

            Glide.with(ivNewsImage.context)
                .load(item?.urlToImage)
                .apply(requestOptions)
                .into(ivNewsImage)
        }
    }

     fun setNewsList(articles: List<ArticlesItem?>?) {
        articles?.let { articlesList.addAll(it) }
    }

    fun reverseList() {
        articlesList.reverse()
        notifyDataSetChanged()
    }
}