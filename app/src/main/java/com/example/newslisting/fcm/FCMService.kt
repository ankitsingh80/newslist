package com.example.newslisting.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.graphics.BitmapFactory
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.newslisting.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FCMService : FirebaseMessagingService() {

    private  val TAG = "FCM"

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.d(TAG, "token: $p0")
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification!!.body)
            createAndShowNotification(remoteMessage)
        }
    }

    private fun createAndShowNotification(remoteMessage: RemoteMessage) {
        val notificationId = 234
        val notificationManager = this.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val channelId = "my_channel_01"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(channelId, "this is my channel", importance)
            mChannel.enableLights(true)
            mChannel.setShowBadge(false)
            notificationManager.createNotificationChannel(mChannel)
        }

        val bitmap = BitmapFactory.decodeResource(this.resources, R.mipmap.ic_launcher)
        val builder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setLargeIcon(bitmap)
            .setContentTitle(remoteMessage.notification?.title ?: "")
            .setContentText(remoteMessage.notification?.body)

        notificationManager.notify(notificationId, builder.build())
    }
}